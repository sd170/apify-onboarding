
exports.makeProductReq = (asinNo) => {
    return {
      url:`https://www.amazon.com/dp/${asinNo}`,
      userData: {
        lable: "PRODUCT",
      },
    }
  };
  
exports.makeOfferReq = (asinNo, uniqueKey) => {
    return {
      url:`https://www.amazon.com/gp/offer-listing/${asinNo}`,
      userData: {
        lable: "OFFER",
      },
      uniqueKey:`${uniqueKey}-offer`
    }
  };


exports.stringToFloat = (str) => {
    return parseFloat(str.replace(/(\D)/g,''))
}