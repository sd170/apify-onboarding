const Apify = require("apify");
const {
  utils: { log, enqueueLinks },
} = Apify;

const { makeProductReq, makeOfferReq, stringToFloat } = require("./utils");

exports.SEARCH = async ({ $, request }, { requestQueue }) => {
  $("div[data-asin]").each(async function (idx, el) {
    if (process.env.ASIN_COUNT && idx >= process.env.ASIN_COUNT) {
      console.log(process.env.ASIN_COUNT, idx);
      // to keep the ASIN number short for edvelopment
      return false;
    }
    const asinNo = $(this).attr("data-asin");
    const asinReqObj = makeProductReq(asinNo);
    asinNo && (await requestQueue.addRequest(asinReqObj)); // only add ASIN request if ASIN not null // some asiNo were empty
  });

  // keeping an empty array so that we acn directly spread and push in it in products route
  const outputKVStore = await Apify.openKeyValueStore(
    process.env.KEY_VALUE_STORAGE_NAME
  );
  await outputKVStore.setValue(process.env.KEY_VALUE_STORAGE_KEY, []);
};

exports.PRODUCT = async ({ $, request }, { requestQueue }) => {
  const url = request.loadedUrl;
  const productTitle = $("#productTitle").text().trim();

  const productDescription = [];
  $("#productDescription_feature_div")
    .contents()
    .each(function () {
      if ($(this).text().trim().length > 0) {
        productDescription.push($(this).text().trim());
      }
    });

  const productInformation = $("#feature-bullets").text().trim();

  const keyword = $(".ac-keyword-link").text();

  const resultObj = {
    url,
    productTitle: productTitle ? productTitle : null,
    productDescription: productDescription ? productDescription.join() : null,
    productInformation: productInformation ? productInformation : null,
    keyword: keyword ? keyword : null,
  };
  // await Apify.pushData({ asinNo, url, productTitle, productInformation });
  // // Open a named key-value store

  // offers
  $("#productDescription_feature_div")
    .contents()
    .each(function () {
      if ($(this).text().trim().length > 0) {
        productDescription.push($(this).text().trim());
      }
    });

  const defaultSellerPrice = $(".apexPriceToPay > span:nth-child(0)").text();
  const defaultSellerName = $("#merchant-info > a:nth-child(2) > span").text();
  const defaultShipping = $("div[id^='mir-layout-DELIVERY_BLOCK-slot'")
    .text()
    .match(/\./g);
  const defaultSellerShipping = defaultShipping
    ? defaultShipping
    : $("#mir-layout-DELIVERY_BLOCK-slot-DELIVERY_MESSAGE")
        .text()
        .match(/free/gi)
    ? "FREE"
    : null;

  let seller1ResultObj = {
    ...resultObj,
    sellerPrice: defaultSellerPrice,
    sellerName: defaultSellerName,
    sellerShipping: defaultSellerShipping,
  };

  const outputKVStore = await Apify.openKeyValueStore(
    process.env.KEY_VALUE_STORAGE_NAME
  );
  const prevValue = await outputKVStore.getValue(
    process.env.KEY_VALUE_STORAGE_KEY
  );
  await outputKVStore.setValue(process.env.KEY_VALUE_STORAGE_KEY, [
    ...prevValue,
    seller1ResultObj,
  ]);

  $(".pa_mbc_on_amazon_offer").each(async function (idx) {
    const sellerPrice = $(this).find("span[id^='mbc-price']").eq(0).text();

    const sellerName = $("span.mbcMerchantName").eq(idx).text().trim();
    const shippingHasNumber = $(this)
      .find("span[id^='mbc-delivery']")
      .eq(0)
      .text()
      .match(/\./g);
    const sellerShipping = shippingHasNumber
      ? shippingHasNumber
      : $(this).find("span[id^='mbc-delivery']").eq(0).text().match(/free/gi)
      ? "FREE"
      : null;

    seller1ResultObj = {
      ...resultObj,
      sellerPrice,
      sellerName,
      sellerShipping,
    };

    const prevValue = await outputKVStore.getValue(
      process.env.KEY_VALUE_STORAGE_KEY
    );
    await outputKVStore.setValue(process.env.KEY_VALUE_STORAGE_KEY, [
      ...prevValue,
      seller1ResultObj,
    ]);
  });

  // const offerReqObj = makeOfferReq(asinNo, request.uniqueKey);
  // await requestQueue.addRequest(offerReqObj);
};

// exports.OFFER = async ({ $, request }, { requestQueue }) => {
//   const asinNo = request.url.split("/").slice(-1)[0];
//   const url = request.loadedUrl;
//   const productTitle = $("#productTitle").text().trim();

//   const productDescription = [];
//   $("#productDescription_feature_div")
//     .contents()
//     .each(function () {
//       if ($(this).text().trim().length > 0) {
//         productDescription.push($(this).text().trim());
//       }
//     });
//   const productInformation = [];
//   $("#feature-bullets")
//     .contents()
//     .each(function () {
//       if ($(this).text().trim().length > 0) {
//         productInformation.push($(this).text().trim());
//       }
//     });
//   // await Apify.pushData({ asinNo, url, productTitle, productInformation });
//   // // Open a named key-value store
//   const outputKVStore = await Apify.openKeyValueStore("OUTPUT");

//   const prevValue = await outputKVStore.getValue("result");
//   if (prevValue === null) {
//     await outputKVStore.setValue("result", [
//       {
//         asinNo: {
//           url,
//           productTitle,
//           productDescription,
//           productInformation,
//         },
//       },
//     ]);
//   } else {
//     await outputKVStore.setValue("result", [
//       ...prevValue,
//       {
//         asinNo: {
//           url,
//           productTitle,
//           productDescription,
//           productInformation,
//         },
//       },
//     ]);
//   }
//   const offerReqObj = makeOfferReq(asinNo, request.uniqueKey);
//   await requestQueue.addRequest(offerReqObj);
// };
