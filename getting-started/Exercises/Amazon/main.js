const Apify = require("apify");
const {
  utils: { log },
} = Apify;
const { takeSearchInput, createRoute } = require("./tools");

Apify.main(async () => {
  // log.setLevel(log.LEVELS.DEBUG);
  log.info("Starting actor.");
  const requestSource = await takeSearchInput();
  log.debug(requestSource);
  const requestQueue = await Apify.openRequestQueue();
  requestQueue.addRequest(requestSource);
  // proxy config
  const proxyConfiguration = await Apify.createProxyConfiguration();

  const router = createRoute({ requestQueue });
  log.info("Crawler setup.");
  const crawler = new Apify.CheerioCrawler({
    requestQueue,
    handlePageFunction: async (context) => {
      // console.log(`Working on ${context.$}`);
      await router(context.request.userData.lable, context);
    },
    maxRequestRetries: 0,
    maxRequestsPerCrawl: 10,
    proxyConfiguration,
    useSessionPool: true, // to use sessions in proxy
    // persistCookiesPerSession: true, // to keep the proxy session
    // additionalMimeTypes:["application/octet-stream"],
    // suggestResponseEncoding:'windows-1250'
  });

  log.info("Crawler call.");
  await crawler.run();
  log.info("Actor finished.");

  const apifyClient = Apify.newClient();
  const user = await apifyClient.user().get();

  const outputKVStore = await Apify.openKeyValueStore(
    process.env.KEY_VALUE_STORAGE_NAME
  );
  const finalResult = await outputKVStore.getValue(
    process.env.KEY_VALUE_STORAGE_KEY
  );

  log.info(`Sending email to ${user.email}...`);
  if (finalResult.length) {
    await Apify.call("apify/send-mail", {
      to: user.email,
      subject: "Amazon Results",
      text: JSON.stringify(finalResult),
    });
  }
});
