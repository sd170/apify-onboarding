const Apify = require("apify");
const {
  utils: { log },
} = Apify;

const routes = require("./routes");

exports.takeSearchInput = async () => {
  const {keyword} = await Apify.getInput();
  const requestSource = {
    url: `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`,
    userData: {
      lable: "SEARCH",
    },
  }
  return requestSource;
};


exports.createRoute = (globalcontext) => {
  return async function (routeName, routeContext) {
    const route = routes[routeName];

    log.info(`Invoking route: ${routeName}`);

    await route(routeContext, globalcontext);
  };
};
