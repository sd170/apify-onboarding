// This is the main Node.js source code file of your actor.

// Import Apify SDK. For more information, see https://sdk.apify.com/
const Apify = require("apify");
const {
    utils: { enqueueLinks },
} = Apify;

Apify.main(async () => {
    // This is same as
    // const input = await Apify.getInput();
    // This
    const defaultKVStore = await Apify.openKeyValueStore("default");
    const input = await defaultKVStore.getValue("INPUT"); // No need for .json ext

    // const reqListSource = [
    //     "https://apify.com/store?category=TRAVEL",
    //     "https://apify.com/store?category=ECOMMERCE",
    //     "https://apify.com/store?category=ENTERTAINMENT",
    // ];
    const reqListSource = input.map((category) => {
        return {
            url: `https://apify.com/store?category=${category}`,
            userData: {
                lable: "CATEGORY",
            },
        };
    });
    console.log("reqListSource", reqListSource);
    const requestList = await Apify.openRequestList(
        "categories",
        reqListSource
    );
    const requestQueue = await Apify.openRequestQueue();

    const crawler = new Apify.CheerioCrawler({
        requestList,
        requestQueue,
        handlePageFunction: async ({ $, request }) => {
            console.log(`We're on ${request.url}`);

            // Scrape from the actor page only
            if (request.userData.lable === "DETAILS") {
                const urlArr = request.url.split("/").slice(-2); // ['apify', 'web-scraper']
                const results = {
                    url: request.url,
                    uniqueIdentifier: urlArr.join("/"), // 'apify/web-scraper'
                    owner: urlArr[0], // 'apify'
                    title: $("header h1").text(),
                    description: $("header span.actor-description").text(),
                    modifiedDate: new Date( // it needs a number not a string
                        Number($("ul.ActorHeader-stats time").attr("datetime"))
                    ),
                    runCount: Number(
                        // $("ul.ActorHeader-stats > li:nth-of-type(3)")
                        $("ul.ActorHeader-stats > li:nth-child(3)")
                            .text()
                            .match(/[\d,]+/)[0] // the matched string is at 0
                            .replace(",", "")
                    ),
                };
                console.log(Object.keys(requestQueue));
                // saving data
                await Apify.pushData(results);
            }

            // enqueue from the default category
            // don't enqueue from the actordetails page
            if ((request.userData.lable = "CATEGORY")) {
                const enqueue = await enqueueLinks({
                    $,
                    requestQueue,
                    selector: "div.item > a", // if pseudoUrls not provided all selectors's href is selected
                    baseUrl: request.loadedUrl,
                    transformRequestFunction: (req) => {
                        req.userData.lable = "DETAILS";
                        return req;
                    },
                });
            }
        },
        maxRequestsPerCrawl: 10,
        maxRequestRetries: 0,
    });

    await crawler.run();
});
