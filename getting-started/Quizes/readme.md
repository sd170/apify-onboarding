# Quizes
## Quiz-1 

- Where and how can you use JQuery with the SDK?
    -  Well, jquery can use the page's HTML directly, once we include the library it can access the HTML from the browser's DOM. But in the case of node, we can't use Jquery, we use cheerio which essentially is jquery, it's just that, as cheerio doesn't have access to the browser it can't get the HTML directly, so we have to provide the HTML to cheerio, that's all. 


- What is the main difference between Cheerio and JQuery?
    - Jqery has access to the browser's DOM so it can direct;y use the browser APIs and of course the HTML. But cheerio doesn't have access to HTML as it runs on Node. So in order to access the HTML for cheerio, it first needs some kind of HTTP client to fetch the HTML. It can be done using node-fetch, axios, etc.


- When would you use CheerioCrawler and what are its limitations?
    - Well, when we know that the page that we're going to scrape sends all (essential) data back at the first response, and doesn't use any javascript to render new data after the initial load, then we can use CheerioCrawler. Altho, apart from this limitation, we should use cheerioCrawler whenever we can as it is 10x faster(as said in the docs) and obviously less CUP exhausting.


- What are the main classes for managing requests and when and why would you use one instead of another?
    - RequestList and RequestQueue are the two classes to manage requests in the Apify ecosystem. 
        - When we know how many pages we want to scrape, and we'll just stick to those pages only throughout the whole scraping timeline, then we'll use **RequestList**. RequestList stores the data in the default key-value store.
        - When we know at least one URL of the page which we're gonna crawl, but later the number of pages can increase, as we go through pages and keep adding URLs for future crawl we'll use **RequestQueue**. Unlike RequestList, RequestQueue uses an SQLite database to keep all the URLs and data needed for scraping them.


- How can you extract data from a page in Puppeteer without using JQuery?
    - Well there is 2 way I know of:
        - Using page.evaluate(), and inside the callBack doing document.querySelector() or document.querySelectorAll() and getting the desired element, and return the data. Thus we get the data from the Chromium's browser environment to our node.
        - Second way is, if we already know which element we wanna extract, we can directly use $eval() [shorthand for document.querySelector()] and as the first parameter pass the CSS selector, and as the 2nd param which is the callback, we can return the data in our desired format.
        If we want to select multiple selectors we can use $$eval, shorthand for document.querySelectorAll().


- What is the default concurrency/parallelism the SDK uses?
    - Well it starts with 1...then quickly goes uo to 2 like that it increases (with the desiredConcurrencyRatio I guess which is 0.95 by default). I've seen it using **context.crawler.autoscaledPool._currentConcurrency** inside the handlePageFunction. Altho we can also set the min and max Concurrency.

**Cheers!**
