// This is the main Node.js source code file of your actor.

// Import Apify SDK. For more information, see https://sdk.apify.com/
const Apify = require("apify");
const { URL } = require("url");
const {
    utils: { enqueueLinks },
} = Apify;

Apify.main(async () => {
    const requestQueue = await Apify.openRequestQueue();
    requestQueue.addRequest({ url: "https://apify.com" });

    const handlePageFunction = async ({ request, $ }) => {
        const pageTitle = $("title").text();
        console.log(`The title of "${request.url}" is: ${pageTitle}.`);

        /* Manual Enqueing */

        // // url after all redirects [req.loadedUrl]
        // const { hostname, origin } = new URL(request.loadedUrl);
        // // getting all urls from a page
        // const newUrls = $("a[href]")
        //     .map((i, anchor) => $(anchor).attr("href"))
        //     .get();
        // // making strings to urls || the 2nd param (request.loadedUrl) is the base...if the 1st param (utl) is relative....it's
        // // appended to 2nd param otherwise, if the 1st param is absolute
        // // the 2nd param (request.loadedUrl) is ignored.
        // const absoluteUrls = newUrls.map((url) => {
        //     // console.log(url);
        //     return new URL(url, request.loadedUrl);
        // });
        // /* My solution: but won't work for subdomains.....eg: dev.apify.com....winn only work for same origin */
        // // const sameDomainUrls = absoluteUrls.map(url => url.hostname === hostname ? url : false)
        // /* Apify solution: but won't work for subdomains.....eg: dev.apify.com....winn only work for same origin */
        // // const sameDomainUrls = absoluteUrls.filter(url => url.origin === origin);

        // /* Apify solution: works for subdomains also */
        // const sameDomainUrls = absoluteUrls.filter((url) =>
        //     url.hostname.endsWith(hostname)
        // );

        // console.log(`Added ${sameDomainUrls.length} more urls.`);

        // // // adding the urls in the requestQueue in series
        // for (url of sameDomainUrls) {
        //     await requestQueue.addRequest({ url: url.href });
        //     console.log(requestQueue.addRequest({ url: url.href }));
        // }

        // implemented parallelization
        // const addRequestPromises = sameDomainUrls.map((url) =>
        //     requestQueue.addRequest({ url: url.href })
        // );
        // await Promise.all(addRequestPromises);

        /* Auto Enqueing with utils */

        const enqueued = await enqueueLinks({
            $,
            requestQueue,
            pseudoUrls: [new RegExp("https?://apify.com/?.*")], // eg was ['http[s?]://apify.com[.*]']  // Ithink [] is ued when theres reguler exp 
            baseUrl: request.loadedUrl, // making relative urls to absolute,
            transformRequestFunction: request => { // can modify the request before putting to enqueue
                request.keepUrlFragment = true; // fragmets: #
                return request;
            }
        });
        console.log(`Added ${enqueued.length} more urls.`);
    };

    const crawler = new Apify.CheerioCrawler({
        maxRequestsPerCrawl: 7,
        requestQueue,
        handlePageFunction,
    });
    await crawler.run();
});
