const Apify = require("apify");
const {
  utils: { log, enqueueLinks },
} = Apify;

exports.CATEGORY = async ({ $, request }, { requestQueue }) => {
  const enqueue = await enqueueLinks({
    $,
    requestQueue,
    selector: "div.item > a", // if pseudoUrls not provided all selectors's href is selected
    baseUrl: request.loadedUrl,
    transformRequestFunction: (req) => {
      req.userData.lable = "DETAILS";
      return req;
    },
  });
};

exports.DETAILS = async ({ $, request }, { requestQueue }) => {
  // scraping from the actor page
  const urlArr = request.url.split("/").slice(-2); // ['apify', 'web-scraper']
  const results = {
    url: request.url,
    uniqueIdentifier: urlArr.join("/"), // 'apify/web-scraper'
    owner: urlArr[0], // 'apify'
    title: $("header h1").text(),
    description: $("header span.actor-description").text(),
    modifiedDate: new Date( // it needs a number not a string
      Number($("ul.ActorHeader-stats time").attr("datetime"))
    ),
    runCount: Number(
      // $("ul.ActorHeader-stats > li:nth-of-type(3)")
      $("ul.ActorHeader-stats > li:nth-child(3)")
        .text()
        .match(/[\d,]+/)[0] // the matched string is at 0
        .replace(",", "")
    ),
  };
//   log.setLevel(log.LEVELS.DEBUG);
  log.debug(JSON.stringify(results));
  // saving data
  await Apify.pushData(results);
};
