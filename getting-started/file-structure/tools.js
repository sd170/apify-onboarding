const Apify = require("apify");
const {
  utils: { log },
} = Apify;

const routes = require("./routes");

exports.takeCategoryUrlInput = async () => {
  const input = await Apify.getInput();
  const requestSource = input.map((category) => {
    return {
      url: `https://apify.com/store?category=${category}`,
      userData: {
        lable: "CATEGORY",
      },
    };
  });
  return requestSource;
};

exports.createRoute = (globalcontext) => {
  return async function (routeName, routeContext) {
    const route = routes[routeName];

    log.info(`Invoking route: ${routeName}`);
    await route(routeContext, globalcontext);
  };
};
