const Apify = require("apify");
const {
  utils: { log },
} = Apify;
const { takeCategoryUrlInput, createRoute } = require("./tools");

Apify.main(async () => {
  log.info("Starting actor.");
  const requestSource = await takeCategoryUrlInput();
  log.info(requestSource);
  const requestList = await Apify.openRequestList("categories", requestSource);
  const requestQueue = await Apify.openRequestQueue();
  // proxy
  const proxyConfiguration = await Apify.createProxyConfiguration({
    groups: ['RESIDENTIAL'],  // optional
    countryCode: 'IN',  
  });

  log.info("Creating a new route");
  const router = createRoute({ requestQueue });
  log.info("Crawler setup.");
  const crawler = new Apify.CheerioCrawler({
    requestList,
    requestQueue,
    handlePageFunction: async (context) => {
      log.info(`Working on ${context.request.url}`);
      // console.log(context.crawler.autoscaledPool._currentConcurrency); 
      await router(context.request.userData.lable, context);

    },
    maxRequestRetries: 0,
    maxRequestsPerCrawl: 10,
    proxyConfiguration,
    useSessionPool: true, // to use sessions in proxy 
    persistCookiesPerSession: true,  // to keep the proxty session
  });

  log.info("Crawler call.");
  await crawler.run();
  log.info("Actor finished.");
});
